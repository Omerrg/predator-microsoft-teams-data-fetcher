from predator_bot.microsoft_teams_client import MicrosoftTeamsClient
import json

if __name__ == '__main__':
    client = MicrosoftTeamsClient()
    teams = client.get_list_of_teams()
    for team in teams:
        team_id = team['id']
        channels = client.get_team_channels(team_id)
        for channel in channels:
            print(client.get_channel_messages(team_id, channel['id']))
