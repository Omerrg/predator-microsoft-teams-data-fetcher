import requests

CLIENT_ID = "6ff0e072-9a2c-4fbb-b6f0-59771b97c332"
CLIENT_SECRET = "]RRPcC901FJ5/OFBOlNtMfZmBvD:NrP["
TENANT_ID = "6055f0f6-f939-4243-9bd0-9f22e8c874c4"
AUTH_TOKEN_URL = "https://login.microsoftonline.com/{TenantID}/oauth2/v2.0/token".format(TenantID=TENANT_ID)


class MicrosoftTeamsClient(object):
    def __init__(self):
        self.auth_token = self.initiate_auth_token()

    def initiate_auth_token(self):
        response = requests.post(AUTH_TOKEN_URL, data={
            "grant_type": "client_credentials",
            "client_id": CLIENT_ID,
            "client_secret": CLIENT_SECRET,
            "scope": "https://graph.microsoft.com/.default"
        })
        return response.json()['access_token']

    def make_graph_call(self, pathname, extract_value=True):
        response = requests.get("http://graph.microsoft.com/v1.0/{pathname}".format(pathname=pathname),
                            headers={"Authorization": "Bearer {auth_token}".format(
                                auth_token=self.auth_token)}).json()
        return response if not extract_value else response['value']

    def get_list_of_teams(self):
        return list(filter(lambda group: 'Team' in group['resourceProvisioningOptions'],
                           self.make_graph_call("groups?$select=id,resourceProvisioningOptions")))

    def get_team_channels(self, team_id):
        return self.make_graph_call("teams/{id}/channels".format(id=team_id))

    def get_channel_messages(self, team_id, channel_id):
        return self.make_graph_call(
            "/teams/{team_id}/channels/{channel_id}/messages".format(team_id=team_id, channel_id=channel_id), extract_value=False)
